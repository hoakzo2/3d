const scrolls = document.getElementById("scrolls");
const imgFolderName = "imgs";
var numberOfImages = 0;
const nevecskem = document.getElementById("nevecskem");

var debug = false;

document.addEventListener("DOMContentLoaded", function() {
    AddImages();
    Update();
});

function Update() {
    ManageBigImage();
    const BigImages = document.getElementById("big_image");
    BigImages.style.display = "block";

    requestAnimationFrame(Update);
}

function Debug(a) {
    if (a != null) {
        debug = a;
        return `Debug is ${a}`;
    } else {
        return debug;
    }
}

var old_img;

function SetBigImage(ihtml, close=false) {
    const BigImages = document.getElementById("big_image");
    const OTHERDIV = document.getElementById("other_div");
    const BigIMG = document.getElementById("big_img");

    if (!close) {
        if (old_img) {
            old_img.style.height = "20vh";
        }
        ihtml.style.height = "15vh";

        BigImages.innerHTML = "";
        var new_img = ihtml.cloneNode();
        new_img.id = "BigImage";
        new_img.style.height = "auto";
        BigImages.appendChild(new_img);
        BigImages.style.display = "block";
        old_img = ihtml;
        OTHERDIV.style.transform = "translateY(200%)";

    } else if (old_img) {
        old_img.style.height = "20vh";
    }
}

function AddImages() {
    const githubRepo = 'https://api.github.com/repos/NemGame/NemGame/contents/imgs';

    fetch(githubRepo)
        .then(response => response.json())
        .then(files => {
            const almaDiv = document.querySelector('.scrolls');
            almaDiv.innerHTML = "";
            files.forEach(file => {
                const imgElement = document.createElement('img');
                imgElement.src = file.download_url;
                imgElement.classList.add("scrolls_kep");
                imgElement.style.height = "20vh";
                imgElement.alt = "Autómatikusan hozzáadott kép";
                imgElement.id = file.download_url;
                imgElement.addEventListener('click', function() {
                    SetBigImage(imgElement);
                });
                almaDiv.appendChild(imgElement);
                if (debug) {
                    console.log(`Új kép hozzáadva: ${imgElement.outerHTML}`);
                }
                numberOfImages++;
            });
        })
        .catch(error => console.error('Error fetching files:', error));
}

function ManageBigImage() {
    const BigImages = document.getElementById("big_image");
    if (BigImages.style.display == "block") {
        document.addEventListener('click', function(event) {
            if (event.target.id == "BigImage") {
                BigImages.style.display = "none";
            }
            console.log(event.target.outerHTML);
        });
    }
}

function ClickedOn(element) {
    const OTHERDIV = document.getElementById("other_div");
    const BigIMG = document.getElementById("big_img");
    if (OTHERDIV.style.display == "block") {
        if (!(element.id == "BigImage")) {
            OTHERDIV.style.transform = "translateY(-200%)";
            SetBigImage(null, true);
        }
    }
}

var latest_key_pressed;

function PressedKey(event="Escape") {
    latest_key_pressed = event.key;
}

function IsKeyPressed(key="Escape") {
    if (latest_key_pressed == key) {
        return true;
    }
    else {
        return false;
    }
}

document.addEventListener('mousedown', function(event) {
    ClickedOn(event.target);
});

document.addEventListener('keydown', function(event) {
    PressedKey(event);
});
